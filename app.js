const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

// Middleware for parsing JSON and form data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Serve static files from the 'public' directory
app.use(express.static('public'));

// Simple homepage
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Form submission endpoint
app.post('/submit', (req, res) => {
  const { firstName, lastName, email, mobileNumber } = req.body;
  
  // Do something with the form data (e.g., store it, process it, etc.)
  console.log('Form submitted:', { firstName, lastName, email, mobileNumber });

  // Respond with a confirmation message
  res.send('Form submitted successfully!');
});

// Start the server
app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});

